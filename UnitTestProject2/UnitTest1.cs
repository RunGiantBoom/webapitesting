﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using WebApiTesting.DataAccess;
using WebApiTesting.Controllers;
//using NUnit.Allure.Attributes;
//using Allure.Commons;
//using AllureCSharpCommons.Attributes;

namespace AllureCSharpCommons.Tests
{
    [TestFixture]
    //[AllureFixture("Description for allure container")]
    public class UserStoreTests {

        [Test]
        public void ConstructorNegativeTest() {
            Assert.Throws<ArgumentNullException>(() => new UserStore(""));
        }
        [Test]
        public void ConstructorTest() {
            var us = new UserStore();
            Assert.IsTrue(true);
        }

        [Test]
        public void FindByNameTest() {
            UserStore us = new UserStore();
            var r = us.FindByNameAsync("player1nick");
            Assert.NotNull(r);
        }
        [Test]
        public void PlayerControllerGetPlayerTest() {
            PlayerController pc = new PlayerController();
            var a = pc.GetPlayer(-1);
            Assert.NotNull(a);
        }

        [TearDown] // вызывается после каждого теста
        public void TearDown() {
            // проверяем статус выполнения текущего теста (вызывается в [TearDown])
            if(TestContext.CurrentContext.Result.Outcome == ResultState.Ignored) // если игнорируется тест и не выполняется (для этого, если нужно, в коде теста пишется "Assert.Ignore()"), то... 
            {
                // КОД (что делать, если тест игнорировался)
            }
            if((TestContext.CurrentContext.Result.Outcome == ResultState.Failure) || (TestContext.CurrentContext.Result.Outcome == ResultState.Error)) // тест завершён с ошибкой
            {
                string error = TestContext.CurrentContext.Result.Message + "\n" + TestContext.CurrentContext.Result.StackTrace; // получить сообщение и стектрейс
                // КОД (что делать, если тест завершён с ошибкой)
            }
            if(TestContext.CurrentContext.Result.Outcome == ResultState.Success) // тест завершён успешно
            {
                // КОД (что делать, если тест завершён успешно)
            }
        }

    }

    //[TestFixture]
    ////[AllureFixture("Description for allure container")]
    //public class Tests
    //{
    //    //[Test]
    //    ////[AllureTest("I'm a test")]
    //    ////[AllureTag("NUnit", "Debug")]
    //    ////[AllureIssue("GitHub#1", "https://github.com/unickq/allure-nunit")]
    //    ////[AllureSeverity(SeverityLevel.minor)]
    //    ////[AllureFeatures("Core")]
    //    //public void EvenTest([Range(0, 5)] int value)
    //    //{
    //    //    Assert.IsTrue(value % 2 == 0, $"Oh no :( {value} % 2 = {value % 2}");
    //    //}
    //}
}
