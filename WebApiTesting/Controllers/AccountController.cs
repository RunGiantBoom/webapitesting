﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebApiTesting.DataAccess.Models;
using WebApiTesting.Models;

namespace WebApiTesting.Controllers
{
    [Authorize]
    public class AccountController : ApiController
    {
        private UserManager<Player> _userManager;
        private IAuthenticationManager _authenticationManager;

        public AccountController()
        {
            _userManager = HttpContext.Current.GetOwinContext().GetUserManager<UserManager<Player>>();
            _authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
        }
        // POST: api/Account/Register
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> Register([FromBody]RegistrationModel value)
        {
            if (value.Password.Equals(value.Confirmpassword) && value.Password.Length >= 8)
            {
                if(value.Nick == null)
                    value.Nick = "";
                //var hash = PasswordManager.CreateHash(value.password); Asp.Identity does this
                var id = Guid.NewGuid();
                var user = new Player(id.ToString(), value.Login, value.Password, value.Nick);
                var result = await _userManager.CreateAsync(user, value.Password);
                if (result.Succeeded)
                {
                    await SignInAsync(user, isPersistent: false);
                    return Ok();
                }
                else
                {
                    return BadRequest("Invalid username or password");
                }
            }
            return BadRequest("Invalid password.");
        }

        // POST: api/Account/Login
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> Login([FromBody]LoginModel value)
        {
            var player = await _userManager.FindAsync(value.login, value.password);

            if (player != null)
            {
                await SignInAsync(player, false);
                return Ok(value.login); //Здесь точно надо редиректить в игру после получения токена. JQuery будет редиректить
            }
            else
                return BadRequest();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IHttpActionResult LogOff()
        {
            _authenticationManager.SignOut();
            return Ok();
        }



        private async Task SignInAsync(Player user, bool isPersistent)
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            _authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }
            base.Dispose(disposing);
        }
    }
}
