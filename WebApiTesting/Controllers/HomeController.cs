﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApiTesting.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            //Index это и есть страница логина по сути
            return View();
        }
        
        public ActionResult Game()
        {
            //try {
            //    string token = Request.ServerVariables.Get("token");
            //    //Верификация токена
            //} catch(Exception e) //Если не удалось получить токен
            //  {
            //    return View("Login");
            //}

            return View("Game");
        }

        public ActionResult Battle()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Magazin()
        {
            return View();
        }
    }
}