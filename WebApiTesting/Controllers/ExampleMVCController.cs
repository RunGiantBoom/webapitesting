﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApiTesting.Controllers
{
    public class ExampleMVCController : Controller
    {
        // GET: ExampleMVC
        public ActionResult Index()
        {
            return View();
        }

        // GET: ExampleMVC/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ExampleMVC/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ExampleMVC/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ExampleMVC/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ExampleMVC/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ExampleMVC/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ExampleMVC/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
