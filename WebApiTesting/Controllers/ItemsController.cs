﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiTesting.Controllers
{
    public class ItemsController : ApiController
    {
        // GET: api/items/types
        [HttpGet]
        public string types()
        {
            return "types";
        }

        // GET: api/items/{id}
        [HttpGet]
        public string Get(int id)
        {
            return "type " + id.ToString();
        }

    }
}
