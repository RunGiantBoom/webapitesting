﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using WebApiTesting.DataAccess;
using WebApiTesting.DataAccess.Models;
using WebApiTesting.Models;

namespace WebApiTesting.Controllers
{
    public class BattlesController : ApiController
    {
        // POST: api/battles/new/
        //[Authorize]
        [HttpPost]
        public IHttpActionResult New() //new с маленькой буквы конфликтует с зарезервированным словом new
        {
            var headers = Request.Headers;
            if (headers.Contains("token"))
            {
                string login = headers.GetValues("token").First();

                //Получение айдишника игрока
                string sql = "SELECT [Id] FROM Players WHERE login=@login";
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@login", login));
                System.Guid thisPlayerId = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
                if (thisPlayerId == null)
                    return BadRequest("Player does not exists");

                //получение айди текущего боя и текущее хп
                sql = "SELECT CurrentBattleId FROM Players WHERE login=@login";
                //Nullable<int> battle_id = DatabaseHelper.ReadOneValue<Nullable<int>>(sql, parameters);
                int battle_id = 0;
                using (var connection = new SqlConnection(DatabaseHelper.connectionString))
                using (var command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.Parameters.Add(new SqlParameter("@login", login));
                    var reader = command.ExecuteReader();
                    reader.Read();
                    object obj = reader.GetValue(0);
                    if (!DBNull.Value.Equals(obj))
                        return BadRequest("Player in battle");
                }

                //Получание айдишника соперника, игрока который не в бою. По идее неплохо бы получать рандомного, но можно и первого.
                sql = "SELECT [Id] FROM Players WHERE login!=@login AND CurrentBattleId IS NULL";
                parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@login", login));
                System.Guid player2id = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
                if (player2id == null)
                    return BadRequest("No Players availible to fight");

                //добавление игрока в бой
                sql = "INSERT INTO Battles (Player1Id, Player2Id, Gold) VALUES (@player1Id, @player2Id, 0);SET @id=SCOPE_IDENTITY()";
                parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@player1Id", thisPlayerId));
                parameters.Add(new SqlParameter("@player2Id", player2id));
                SqlParameter idParam = new SqlParameter// параметр для id боя
                {
                    ParameterName = "@id",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output // параметр выходной
                };
                parameters.Add(idParam);
                //Выполняем запрос и получаем на выход айди созданного боя
                DatabaseHelper.ExecuteNonQuery(sql, parameters);
                battle_id = (int)idParam.Value;

                sql = "UPDATE Players SET [CurrentBattleId] = @battle_id WHERE Id = @player1 OR Id = @player2";
                parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@player1", thisPlayerId));
                parameters.Add(new SqlParameter("@player2", player2id));
                parameters.Add(new SqlParameter("@battle_id", battle_id));
                //Здесь будет добавление записи в бд
                DatabaseHelper.ExecuteNonQuery(sql, parameters);
                return Ok(battle_id.ToString());
            }
            return BadRequest();//здесь нужно возвращать unauthorize result
        }

        // POST: api/battles/move
        [HttpPost]
        public IHttpActionResult move([FromBody]HitModel move) //должно принимать на вход точку удара, защиты и позицию.
        {
            if (move.AttackTarget < 0 || move.AttackTarget >= 4)
                return BadRequest("Attack must be between 0 and 4");
            if (move.DefenceTarget < 0 || move.DefenceTarget >= 4)
                return BadRequest("Defence must be between 0 and 4");

            var headers = Request.Headers;
            if (headers.Contains("token"))
            {
                string login = headers.GetValues("token").First();

                //Теперь. Берём логин игрока, запрашиваем в бд в бою ли он.
                string sql = "SELECT [Id] FROM Players WHERE login=@login AND CurrentBattleId IS NOT NULL";
                List<SqlParameter> parameters = new List<SqlParameter>() { new SqlParameter("@login", login) };
                System.Guid thisPlayerId = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
                if (thisPlayerId == null)
                    return BadRequest("Player does not exists or he is not in battle");

                //По идее можно и прямо сразу запросить айди боя по логину.
                sql = "SELECT CurrentBattleId FROM Players WHERE login=@login";
                parameters = new List<SqlParameter>() { new SqlParameter("@login", login) };
                long battleid = DatabaseHelper.ReadOneValue<long>(sql, parameters);

                sql = "SELECT Health FROM Players WHERE login=@login";
                parameters = new List<SqlParameter>() { new SqlParameter("@login", login) };
                int thisPlayerHealth = DatabaseHelper.ReadOneValue<int>(sql, parameters);

                //Второй игрок. Ты можешь быть вторым игроком, поэтому надо ещё проверять, если второй игрок ты, то взять первого игрока
                sql = "SELECT [Player2Id] FROM Battles WHERE Id=@battleid";
                parameters = new List<SqlParameter>() { new SqlParameter("@battleid", battleid) };
                System.Guid enemyPlayerId = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
                bool isPlayer1 = true;
                if (enemyPlayerId.Equals(thisPlayerId))
                {
                    sql = "SELECT [player1id] FROM Battles WHERE Id=@battleid";
                    parameters = new List<SqlParameter>() { new SqlParameter("@battleid", battleid) };
                    enemyPlayerId = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters); //Соперник записывается как плеер 1
                    isPlayer1 = false;
                }
                sql = "SELECT Health FROM Players WHERE Id=@Id";
                parameters = new List<SqlParameter>() { new SqlParameter("@Id", enemyPlayerId) };
                int enemyPlayerhealth = DatabaseHelper.ReadOneValue<int>(sql, parameters);

                //Теперь нужно собрать полностью характеристики игрока и соперника
                //А значит нужно получить WornItemsId отдельно каждого игрока
                //int player1_worn_items_id;
                //sql = "SELECT [WornItemsId] FROM Players WHERE Id=@player1id";
                //parameters = new List<SqlParameter>() { new SqlParameter("@player1id", player1id) };
                //player1_worn_items_id = DatabaseHelper.ReadOneValue<int>(sql, parameters);

                //int player2_worn_items_id;
                //sql = "SELECT [WornItemsId] FROM Players WHERE Id=@player2id";
                //parameters = new List<SqlParameter>() { new SqlParameter("@player2id", player2id) };
                //player2_worn_items_id = DatabaseHelper.ReadOneValue<int>(sql, parameters);

                //Получить соответсвующий список айдишников всех вещей
                //Для каждого айдишника перебрать базу с предметами и суммировать все характеристики в один объект.
                //Жесть какая. Сейчас будет по селекту на каждый айтем.
                ItemStats thisPlayerItemsSum = new ItemStats();
                ItemStats enemyPlayerItemsSum = new ItemStats();
                for (int playeritems = 0; playeritems < 2; playeritems++)
                {
                    ItemStats curitems;
                    if (playeritems == 0)
                        curitems = thisPlayerItemsSum;
                    else
                        curitems = enemyPlayerItemsSum;

                    int[] itemids = new int[16];
                    sql = "SELECT [HelmetId], [ArmorId], [BeltId], [ShoesId], [Ring1Id], [Ring2Id], [Ring3Id], [Ring4Id], [Ring5Id], [Ring6Id], [Ring7Id], [Ring8Id], [Ring9Id], [Ring10Id], [WeaponLeftId], [WeaponRightId] FROM WornItemsHistory WHERE PlayerId=@playerid";
                    using (var connection = new SqlConnection(DatabaseHelper.connectionString))
                    using (var command = new SqlCommand(sql, connection))
                    {
                        connection.Open();
                        if (playeritems == 0)
                            command.Parameters.Add(new SqlParameter("@playerid", thisPlayerId));
                        else
                            command.Parameters.Add(new SqlParameter("@playerid", enemyPlayerId));
                        var reader = command.ExecuteReader();
                        reader.Read();
                        for (int i = 0; i < 16; i++) //16 предметов
                        {
                            itemids[i] = (DBNull.Value.Equals(reader.GetValue(i)) == true) ? 0 : reader.GetInt32(i);
                        }
                    }
                    sql = "SELECT [Damage] ,[Armor] ,[Critical] ,[Anticritical] ,[Dodge] ,[Antidodge] FROM Items WHERE Id=@itemid";
                    using (var connection = new SqlConnection(DatabaseHelper.connectionString))
                    {
                        connection.Open();
                        for (int i = 0; i < 16; i++)
                        {
                            if (itemids[i] == 0) continue;
                            using (var command = new SqlCommand(sql, connection))
                            {
                                command.Parameters.Add(new SqlParameter("@itemid", itemids[i]));
                                var reader = command.ExecuteReader();
                                reader.Read();
                                curitems.Damage += reader.GetInt32(0);
                                curitems.Armor += reader.GetInt32(1);
                                curitems.Critical += reader.GetDouble(2);
                                curitems.Anticritical += reader.GetDouble(3);
                                curitems.Dodge += reader.GetDouble(4);
                                curitems.Antidodge += reader.GetDouble(5);
                                reader.Close();
                            }
                        }
                    }
                }
                //Так. Пережили. Теперь у нас есть два игрока с известным списком характеристик
                //Ищем, если ли ход в истории боев, на который мы можем ответить. Если да, то отвечаем на него, иначе делаем новый ход.
                //Так же. Если у соперника есть неотвеченный ход, то мы ещё не можем ходить.
                if (isPlayer1)
                    sql = "SELECT [AttackPosition2] ,[DefensePosition2] FROM BattleSteps WHERE battleid=@battleid AND DefensePosition1 IS NULL";
                else
                    sql = "SELECT [AttackPosition1] ,[DefensePosition1] FROM BattleSteps WHERE battleid=@battleid AND DefensePosition2 IS NULL";
                int enemyAttackPosition = 0;
                int enemyDefencePosition = 0;
                using (var connection = new SqlConnection(DatabaseHelper.connectionString))
                using (var command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.Parameters.Add(new SqlParameter("@battleid", battleid));
                    var reader = command.ExecuteReader();
                    if (reader.Read()) //Враг уже сделал ход, нужно посчитать свои и его статы и вынести вердикт по результатам хода.
                    {
                        Random r = new Random();
                        //Сначала нужно проверить, имеется ли уворот. 
                        //Потом есть ли крит.
                        //После чего проверить точки попадания и высчитать урон.
                        int avoidChanceOfPlayer1 = (int)(thisPlayerItemsSum.Dodge - enemyPlayerItemsSum.Antidodge);
                        int avoidChanceOfPlayer2 = (int)(enemyPlayerItemsSum.Dodge - thisPlayerItemsSum.Antidodge);
                        bool avoidOfPlayer1 = r.Next(100) < avoidChanceOfPlayer1;
                        bool avoidOfPlayer2 = r.Next(100) < avoidChanceOfPlayer2;

                        int critChanceOfPlayer1 = (int)(thisPlayerItemsSum.Critical - enemyPlayerItemsSum.Anticritical);
                        int critChanceOfPlayer2 = (int)(enemyPlayerItemsSum.Critical - thisPlayerItemsSum.Anticritical);
                        bool critOfPlayer1 = r.Next(100) < critChanceOfPlayer1;
                        bool critOfPlayer2 = r.Next(100) < critChanceOfPlayer2;

                        int player1CalculatedDamage = thisPlayerItemsSum.Damage - enemyPlayerItemsSum.Armor;
                        int player2CalculatedDamage = enemyPlayerItemsSum.Damage - thisPlayerItemsSum.Armor;

                        int player1damage = 0;
                        int player1defence = 0;
                        int player2damage = 0;
                        int player2defence = 0;

                        enemyAttackPosition = reader.GetInt32(0);
                        enemyDefencePosition = reader.GetInt32(1);
                        reader.Close();

                        if (!avoidOfPlayer2)
                            if (move.AttackTarget == enemyDefencePosition)
                                if (critOfPlayer1) //игрок1 пробил блок. броня врага всё ещё работает
                                    player1damage = player1CalculatedDamage;
                                else //Соперник заблокировал удар
                                    player2defence = player1CalculatedDamage;
                            else
                                if (critOfPlayer1) //критический удар
                                player1damage = 2 * player1CalculatedDamage;
                            else //обычный удар
                                player1damage = player1CalculatedDamage;

                        if (!avoidOfPlayer1)
                            if (move.DefenceTarget == enemyAttackPosition)
                                if (critOfPlayer2) //враг пробил блок
                                    player2damage = player2CalculatedDamage;
                                else //удар заблокирован
                                    player2defence = player2CalculatedDamage;
                            else
                                if (critOfPlayer2) //критический удар
                                player2damage = 2 * player2CalculatedDamage;
                            else //обычный удар
                                player2damage = player2CalculatedDamage;

                        if (isPlayer1)
                            sql = "UPDATE BattleSteps " +
                                "SET AttackPosition1 = @attackPosition, DefensePosition1 = @DefensePosition, HPOnStart1 = @hpOStart," +
                                "Damage1 = @damage1, Protection1 = @protection1, Crit1 = @crit1, Avoid1 = @avoid1, " +
                                "Damage2 = @damage2, Protection2 = @protection2, Crit2 = @crit2, Avoid2 = @avoid2 " +
                                "WHERE [BattleId] = @battle_id";
                        else
                            sql = "UPDATE BattleSteps " +
                                "SET AttackPosition2 = @attackPosition, DefensePosition2 = @DefensePosition, HPOnStart2 = @hpOStart, " +
                                "Damage1 = @damage1, Protection1 = @protection1, Crit1 = @crit1, Avoid1 = @avoid1, " +
                                "Damage2 = @damage2, Protection2 = @protection2, Crit2 = @crit2, Avoid2 = @avoid2 " +
                                "WHERE [BattleId] = @battle_id";
                        parameters = new List<SqlParameter>();
                        parameters.Add(new SqlParameter("@attackPosition", move.AttackTarget));
                        parameters.Add(new SqlParameter("@DefensePosition", move.DefenceTarget));
                        parameters.Add(new SqlParameter("@hpOStart", thisPlayerHealth));
                        parameters.Add(new SqlParameter("@damage1", player1damage));
                        parameters.Add(new SqlParameter("@protection1", player1defence));
                        parameters.Add(new SqlParameter("@crit1", critOfPlayer1));
                        parameters.Add(new SqlParameter("@avoid1", avoidOfPlayer1));
                        parameters.Add(new SqlParameter("@damage2", player2damage));
                        parameters.Add(new SqlParameter("@protection2", player2defence));
                        parameters.Add(new SqlParameter("@crit2", critOfPlayer2));
                        parameters.Add(new SqlParameter("@avoid2", avoidOfPlayer2));
                        parameters.Add(new SqlParameter("@battle_id", battleid));
                        DatabaseHelper.ExecuteNonQuery(sql, parameters);


                        thisPlayerHealth -= player2damage;
                        enemyPlayerhealth -= player1damage;
                        bool player1win = false;
                        bool player2win = false;
                        //Здесь так же нужно уменьшать хп у обоих игроков, и заканчивать бой если у одного из них хп вышло меньше 0
                        string sql2 = "";
                        if (thisPlayerHealth < 0) //победил соперник
                        {
                            player2win = true;
                        }
                        else if (enemyPlayerhealth < 0) //победили вы
                        {
                            player1win = true;
                        }
                        else //просто добавляем обновлённые хп в свою базу
                        {
                            sql = "UPDATE Players SET Health = @health WHERE Id = @playerId";
                            parameters = new List<SqlParameter>();
                            parameters.Add(new SqlParameter("@health", thisPlayerHealth));
                            parameters.Add(new SqlParameter("@playerid", thisPlayerId));
                            DatabaseHelper.ExecuteNonQuery(sql, parameters);

                            sql = "UPDATE Players SET Health = @health WHERE Id = @playerId";
                            parameters = new List<SqlParameter>();
                            parameters.Add(new SqlParameter("@health", enemyPlayerhealth));
                            parameters.Add(new SqlParameter("@playerid", enemyPlayerId));
                            DatabaseHelper.ExecuteNonQuery(sql, parameters);
                        }
                        if (player1win || player2win)
                        {
                            sql = "UPDATE Players SET Health = 100, CurrentBattleId = NULL WHERE Id = @player1Id OR Id = @player2Id";
                            parameters = new List<SqlParameter>();
                            parameters.Add(new SqlParameter("@player1id", thisPlayerId));
                            parameters.Add(new SqlParameter("@player2id", enemyPlayerId));
                            DatabaseHelper.ExecuteNonQuery(sql, parameters);

                            sql = "UPDATE Battles SET IsWinner1 = @iswinner1 WHERE ID = @Id";
                            parameters = new List<SqlParameter>();
                            parameters.Add(new SqlParameter("@iswinner1", player1win));
                            parameters.Add(new SqlParameter("@Id", battleid));
                            DatabaseHelper.ExecuteNonQuery(sql, parameters);
                            if (player1win)
                                return Ok("Battle ends. Player 1 win");
                            else
                                return Ok("Battle ends. Player 2 win");
                        }
                        else
                        {
                            return Ok("Move accepted. Both players calculated");
                        }
                    }
                    else //У врага нет незавершенного хода
                    {
                        reader.Close();
                        //Вычисляем какой будет номер шага.
                        int step = -1;
                        sql = "SELECT StepNum FROM BattleSteps WHERE BattleId=@battle_id AND AttackPosition1 IS NOT NULL AND AttackPosition2 IS NOT NULL";
                        using (var command2 = new SqlCommand(sql, connection))
                        {
                            command2.Parameters.Add(new SqlParameter("@battle_id", battleid));
                            var reader2 = command2.ExecuteReader();
                            while(reader2.Read())
                                step = reader2.GetInt32(0);
                            step += 1;
                            reader2.Close();
                        }
                        if (isPlayer1)
                            sql = "INSERT INTO BattleSteps (AttackPosition1, DefensePosition1, HPOnStart1, BattleId, StepNum) VALUES (@attackPosition, @DefensePosition, @HPOnStart, @battle_id, @StepNum)"; //Номер шага захардкожен
                        else
                            sql = "INSERT INTO BattleSteps (AttackPosition2, DefensePosition2, HPOnStart2, BattleId, StepNum) VALUES (@attackPosition, @DefensePosition, @HPOnStart, @battle_id, @StepNum)";

                        parameters = new List<SqlParameter>();
                        parameters.Add(new SqlParameter("@attackPosition", move.AttackTarget));
                        parameters.Add(new SqlParameter("@DefensePosition", move.DefenceTarget));
                        parameters.Add(new SqlParameter("@HPOnStart", thisPlayerHealth));
                        parameters.Add(new SqlParameter("@battle_id", battleid));
                        parameters.Add(new SqlParameter("@StepNum", step));
                        //Здесь будет добавление записи в бд
                        DatabaseHelper.ExecuteNonQuery(sql, parameters);
                        return Ok("Move accepted");
                    }
                }
            }
            return Ok("Last exit from move function");
        }

        // POST: api/battles/status
        [HttpGet]
        public string status() //Возвращает статус последнего завершенного хода. Необходим чтобы проверять, сходил враг или нет.
        {
            return "status";
        }

        // POST: api/battles/runaway
        [HttpPost]
        public string runaway()
        {
            //Находим игрока, если он в бою то убираем его из боя
            //Аналогично убираем из боя второго игрока
            //В таблице battles назначаем победителем второго игрока
            return "battles runaway";
        }

        // GET: api/battles/{id}
        [HttpGet]
        public string Get(int id)
        {
            return "battles value";
        }
    }
}
