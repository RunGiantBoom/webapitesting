﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using WebApiTesting.DataAccess;
using WebApiTesting.DataAccess.Models;
using WebApiTesting.Models;

namespace WebApiTesting.Controllers
{
    public class PlayerController : ApiController
    {
        //helpful methods
        private string GetPlayerLogin(HttpRequestHeaders headers) {
            //получение логина.
            if(!headers.Contains("token"))
                return null;
            return headers.GetValues("token").First();
        }

        private System.Guid GetPlayerId(string login) {
            //Получение айдишника игрока
            string sql = "SELECT [Id] FROM Players WHERE login=@login";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@login", login));
            return DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
        }


        // POST: api/player/wear/{itemId}
        [HttpPost]
        [Route("api/player/wear/{itemId:int}")]
        public IHttpActionResult wear(int itemId) //Получаем здесь конкретный айдишник предмета
        {
            //получение логина.
            //var headers = Request.Headers;
            //if (!headers.Contains("token"))
            //    return BadRequest("Token required");
            //string login = headers.GetValues("token").First();
            string login = GetPlayerLogin(Request.Headers);

            //Получение айдишника игрока
            //string sql = "SELECT [Id] FROM Players WHERE login=@login";
            //List<SqlParameter> parameters = new List<SqlParameter>();
            //parameters.Add(new SqlParameter("@login", login));
            //System.Guid player1id = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
            System.Guid player1id = GetPlayerId(login);
            if (player1id == null)                          //player1id == System.Guid.Empty ??
                return BadRequest("Player does not exists");

            //Ищем, существует ли такой предмет в базе
            //пока без этого

            //Добавление предмета к игроку в таблице weareditems, с добавлением дополнительной строчки
            //В какую ячейку добавлять? в ту где null
            string sql = "SELECT [HelmetId], [ArmorId], [BeltId], [ShoesId], [Ring1Id], [Ring2Id], [Ring3Id], [Ring4Id], [Ring5Id], [Ring6Id], [Ring7Id], [Ring8Id], [Ring9Id], [Ring10Id], [WeaponLeftId], [WeaponRightId] FROM WornItemsHistory WHERE PlayerId=@playerid";
            using (var connection = new SqlConnection(DatabaseHelper.connectionString))
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.Parameters.Add(new SqlParameter("@playerid", player1id));
                    var reader = command.ExecuteReader();
                    reader.Read();
                    string update_column = "";
                    if (DBNull.Value.Equals(reader.GetValue(0))) update_column = "HelmetId";
                    else if (DBNull.Value.Equals(reader.GetValue(1))) update_column = "ArmorId";
                    else if (DBNull.Value.Equals(reader.GetValue(2))) update_column = "BeltId";
                    else if (DBNull.Value.Equals(reader.GetValue(3))) update_column = "ShoesId";
                    else if (DBNull.Value.Equals(reader.GetValue(4))) update_column = "Ring1Id";
                    else if (DBNull.Value.Equals(reader.GetValue(5))) update_column = "Ring2Id";
                    else if (DBNull.Value.Equals(reader.GetValue(6))) update_column = "Ring3Id";
                    else if (DBNull.Value.Equals(reader.GetValue(7))) update_column = "Ring4Id";
                    else if (DBNull.Value.Equals(reader.GetValue(8))) update_column = "Ring5Id";
                    else if (DBNull.Value.Equals(reader.GetValue(9))) update_column = "Ring6Id";
                    else if (DBNull.Value.Equals(reader.GetValue(10))) update_column = "Ring7Id";
                    else if (DBNull.Value.Equals(reader.GetValue(11))) update_column = "Ring8Id";
                    else if (DBNull.Value.Equals(reader.GetValue(12))) update_column = "Ring9Id";
                    else if (DBNull.Value.Equals(reader.GetValue(13))) update_column = "Ring10Id";
                    else if (DBNull.Value.Equals(reader.GetValue(14))) update_column = "WeaponLeftId";
                    else if (DBNull.Value.Equals(reader.GetValue(15))) update_column = "WeaponRightId";
                    else return BadRequest("No availible slots for item");
                    sql = "UPDATE WornItemsHistory SET " + update_column + " = @itemid WHERE PlayerId=@playerid";
                    reader.Close();
                }

                using (var command = new SqlCommand(sql, connection))
                {
                    command.Parameters.Add(new SqlParameter("@itemid", itemId));
                    command.Parameters.Add(new SqlParameter("@playerid", player1id));
                    command.ExecuteNonQuery();
                    return Ok("item weared");
                }
            }

            return Ok();
        }

        


        // POST: api/player/remove/{itemId}
        [HttpPost]
        [Route("api/player/remove/{itemId:int}")]
        public IHttpActionResult remove(int itemId)
        {
            //получение логина.
            //var headers = Request.Headers;
            //if (!headers.Contains("token"))
            //    return BadRequest("Token required");
            //string login = headers.GetValues("token").First();
            string login = GetPlayerLogin(Request.Headers);

            //Получение айдишника игрока
            //string sql = "SELECT [Id] FROM Players WHERE login=@login";
            //List<SqlParameter> parameters = new List<SqlParameter>();
            //parameters.Add(new SqlParameter("@login", login));
            //System.Guid player1id = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
            System.Guid player1id = GetPlayerId(login);
            if (player1id == null)
                return BadRequest("Player does not exists");

            string sql = "SELECT [HelmetId], [ArmorId], [BeltId], [ShoesId], [Ring1Id], [Ring2Id], [Ring3Id], [Ring4Id], [Ring5Id], [Ring6Id], [Ring7Id], [Ring8Id], [Ring9Id], [Ring10Id], [WeaponLeftId], [WeaponRightId] FROM WornItemsHistory WHERE PlayerId=@playerid";
            using (var connection = new SqlConnection(DatabaseHelper.connectionString))
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.Parameters.Add(new SqlParameter("@playerid", player1id));
                    var reader = command.ExecuteReader();
                    reader.Read();
                    string update_column = "";

                    if (!DBNull.Value.Equals(reader.GetValue(0)) && itemId == reader.GetInt32(0)) update_column = "HelmetId";
                    else if (!DBNull.Value.Equals(reader.GetValue(1)) && itemId == reader.GetInt32(1)) update_column = "ArmorId";
                    else if (!DBNull.Value.Equals(reader.GetValue(2)) && itemId == reader.GetInt32(2)) update_column = "BeltId";
                    else if (!DBNull.Value.Equals(reader.GetValue(3)) && itemId == reader.GetInt32(3)) update_column = "ShoesId";
                    else if (!DBNull.Value.Equals(reader.GetValue(4)) && itemId == reader.GetInt32(4)) update_column = "Ring1Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(5)) && itemId == reader.GetInt32(5)) update_column = "Ring2Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(6)) && itemId == reader.GetInt32(6)) update_column = "Ring3Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(7)) && itemId == reader.GetInt32(7)) update_column = "Ring4Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(8)) && itemId == reader.GetInt32(8)) update_column = "Ring5Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(9)) && itemId == reader.GetInt32(9)) update_column = "Ring6Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(10)) && itemId == reader.GetInt32(10)) update_column = "Ring7Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(11)) && itemId == reader.GetInt32(11)) update_column = "Ring8Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(12)) && itemId == reader.GetInt32(12)) update_column = "Ring9Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(13)) && itemId == reader.GetInt32(13)) update_column = "Ring10Id";
                    else if (!DBNull.Value.Equals(reader.GetValue(14)) && itemId == reader.GetInt32(14)) update_column = "WeaponLeftId";
                    else if (!DBNull.Value.Equals(reader.GetValue(15)) && itemId == reader.GetInt32(15)) update_column = "WeaponRightId";
                    else return BadRequest("All items already removed"); //"No items with such id are worn"

                    sql = "UPDATE WornItemsHistory SET " + update_column + " = NULL WHERE PlayerId=@playerid";
                    reader.Close();
                }

                using (var command = new SqlCommand(sql, connection))
                {
                    command.Parameters.Add(new SqlParameter("@playerid", player1id));
                    command.ExecuteNonQuery();
                    return Ok("item removed");
                }
            }
            return BadRequest();
        }


        [HttpGet]
        //[Route("wear/{itemId:int}")]
        public WornItems weared() //Получаем здесь конкретный айдишник предмета
        {
            //получение логина.
            //var headers = Request.Headers;
            //if (!headers.Contains("token"))
            //    return null;
            //string login = headers.GetValues("token").First();
            string login = GetPlayerLogin(Request.Headers);

            //Получение айдишника игрока
            //string sql = "SELECT [Id] FROM Players WHERE login=@login";
            //List<SqlParameter> parameters = new List<SqlParameter>();
            //parameters.Add(new SqlParameter("@login", login));
            //System.Guid player1id = DatabaseHelper.ReadOneValue<System.Guid>(sql, parameters);
            System.Guid player1id = GetPlayerId(login);
            if(player1id == null)
                return null;

            WornItems wornItems = new WornItems();

            string sql = "SELECT [HelmetId], [ArmorId], [BeltId], [ShoesId], [Ring1Id], [Ring2Id], [Ring3Id], [Ring4Id], [Ring5Id], [Ring6Id], [Ring7Id], [Ring8Id], [Ring9Id], [Ring10Id], [WeaponLeftId], [WeaponRightId] FROM WornItemsHistory WHERE PlayerId=@playerid";
            using(var connection = new SqlConnection(DatabaseHelper.connectionString))
                using(var command = new SqlCommand(sql, connection)) {
                    connection.Open();
                    command.Parameters.Add(new SqlParameter("@playerid", player1id));
                    var reader = command.ExecuteReader();
                    reader.Read();
                    wornItems.helmetId = (DBNull.Value.Equals(reader.GetValue(0)) == true) ? 0 : reader.GetInt32(0);
                    wornItems.armorId = (DBNull.Value.Equals(reader.GetValue(1)) == true) ? 0 : reader.GetInt32(1);
                    wornItems.beltId = (DBNull.Value.Equals(reader.GetValue(2)) == true) ? 0 : reader.GetInt32(2);
                    wornItems.shoesId = (DBNull.Value.Equals(reader.GetValue(3)) == true) ? 0 : reader.GetInt32(3);
                    wornItems.ring1Id = (DBNull.Value.Equals(reader.GetValue(4)) == true) ? 0 : reader.GetInt32(4);
                    wornItems.ring2Id = (DBNull.Value.Equals(reader.GetValue(5)) == true) ? 0 : reader.GetInt32(5);
                    wornItems.ring3Id = (DBNull.Value.Equals(reader.GetValue(6)) == true) ? 0 : reader.GetInt32(6);
                    wornItems.ring4Id = (DBNull.Value.Equals(reader.GetValue(7)) == true) ? 0 : reader.GetInt32(7);
                    wornItems.ring5Id = (DBNull.Value.Equals(reader.GetValue(8)) == true) ? 0 : reader.GetInt32(8);
                    wornItems.ring6Id = (DBNull.Value.Equals(reader.GetValue(9)) == true) ? 0 : reader.GetInt32(9);
                    wornItems.ring7Id = (DBNull.Value.Equals(reader.GetValue(10)) == true) ? 0 : reader.GetInt32(10);
                    wornItems.ring8Id = (DBNull.Value.Equals(reader.GetValue(11)) == true) ? 0 : reader.GetInt32(11);
                    wornItems.ring9Id = (DBNull.Value.Equals(reader.GetValue(12)) == true) ? 0 : reader.GetInt32(12);
                    wornItems.ring10Id = (DBNull.Value.Equals(reader.GetValue(13)) == true) ? 0 : reader.GetInt32(13);
                    wornItems.leftweaponId = (DBNull.Value.Equals(reader.GetValue(14)) == true) ? 0 : reader.GetInt32(14);
                    wornItems.rightweaponId = (DBNull.Value.Equals(reader.GetValue(15)) == true) ? 0 : reader.GetInt32(15);
                }
            return wornItems;
        }

        // POST: api/player/buy/{itemTypeId}
        [HttpPost]
        //[Route("buy/{itemTypeId:int}")]
        public string buy(int itemTypeId)
        {
            return "buy " + itemTypeId.ToString();
        }

        // POST: api/player/sell/{id}
        [HttpPost]
        [Authorize()]
        public IHttpActionResult sell(int id)
        {
            var u = User;
            return Ok(u);
            //return "sell " + id.ToString();
        }

        // GET: api/player/inventory/{id}
        [HttpGet]
        [Authorize()]
        public string inventory(int id)
        {
            var u = User;
            return "inventory " + id.ToString() + "!" + User.Identity.Name;
        }

        // GET: api/player/{id}
        [HttpGet]
        [Route("api/player/{id:int}")]
        public Player GetPlayer(int id)
        {
            //return "playerid: " + id.ToString();
            //return null; //return new Player(1,"asd",0,0,new byte[10], 0, 100, 10);

            string userName = "";

            if(id == -1)
                userName = "player1";
            else
                userName = GetPlayerLogin(Request.Headers);

            if(string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException(nameof(userName));

            Player player = null;
            using(var connection = new SqlConnection(DatabaseHelper.connectionString))
            using(var command = connection.CreateCommand()) {
                connection.Open();
                command.CommandText = "SELECT Id, Login, PasswordHash, Nickname, Gold, Experience, " +
                                      "ImagePath, CurrentBattleId, Health, WornItemsId, SecurityStamp " +
                                      "FROM [dbo].[Players] " +
                                      "WHERE Login LIKE @UserName";

                var parameter = new SqlParameter {
                    ParameterName = "@UserName",
                    Value = userName
                };
                command.Parameters.Add(parameter);
                var reader = command.ExecuteReader();
                if(reader.Read()) {
                    player = new Player(reader.GetGuid(0).ToString(), reader.GetString(1), reader.GetString(2),
                        reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5), reader.GetString(6),
                        reader.IsDBNull(7) ? (long?)null : reader.GetInt64(7), reader.GetInt32(8), reader.GetInt64(9),
                        reader.IsDBNull(10) ? "" : reader.GetString(10));
                }

                return player;
            }
        }

        // GET: api/player/{id}/history
        [HttpGet]
        [Route("{id:int}/history")]
        public string history(int id)
        {
            return "history";
        }
    }
}
