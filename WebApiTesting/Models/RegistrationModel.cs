﻿namespace WebApiTesting.Models
{
    public class RegistrationModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Confirmpassword { get; set; }
        public string Nick { get; set; }
    }
}