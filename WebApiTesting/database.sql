CREATE DATABASE [WebApiTesting]
GO
USE [WebApiTesting]


CREATE TABLE [dbo].[Battles](
	[Id] [bigint] NOT NULL IDENTITY(1,1),
	[Player1Id] [uniqueidentifier] NOT NULL,
	[Player2Id] [uniqueidentifier] NOT NULL,
	[IsWinner1] [bit] NULL,
	[Gold] [money] NOT NULL,
 CONSTRAINT [PK_Battles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[BattleSteps](
	[BattleId] [bigint] NOT NULL,
	[StepNum] [int] NOT NULL,
	[HPOnStart1] [int] NULL,
	[AttackPosition1] [int] NULL,
	[DefensePosition1] [int] NULL,
	[Damage1] [int] NULL,
	[Protection1] [int] NULL,
	[Crit1] [bit] NULL,
	[Avoid1] [bit] NULL,
	[HPOnStart2] [int] NULL,
	[AttackPosition2] [int] NULL,
	[DefensePosition2] [int] NULL,
	[Damage2] [int] NULL,
	[Protection2] [int] NULL,
	[Crit2] [bit] NULL,
	[Avoid2] [bit] NULL,
 CONSTRAINT [PK_BattleSteps] PRIMARY KEY CLUSTERED 
(
	[BattleId] ASC,
	[StepNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Inventory](
	[PlayerId] [uniqueidentifier] NOT NULL,
	[ItemId] [int] NOT NULL,
 CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED 
(
	[PlayerId] ASC,
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Items](
	[Id] [int] NOT NULL IDENTITY(1,1) ,
	[Name] [nvarchar](50) NOT NULL,
	[TypeId] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[ImagePath] [nvarchar](50) NOT NULL,
	[Damage] [int] NOT NULL,
	[Armor] [int] NOT NULL,
	[Critical] [float] NOT NULL,
	[Anticritical] [float] NOT NULL,
	[Dodge] [float] NOT NULL,
	[Antidodge] [float] NOT NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ItemTypes](
	[Id] [int] NOT NULL IDENTITY(1,1) ,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ItemTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Players](
	[Id] [uniqueidentifier] NOT NULL,
	[Login] [nvarchar](255) NOT NULL,
	[PasswordHash] [nvarchar](128) NOT NULL,
	[Nickname] [nvarchar](20) NOT NULL,
	[Gold] [int] NOT NULL,
	[Experience] [int] NOT NULL,
	[ImagePath] [nvarchar](50) NOT NULL,
	[CurrentBattleId] [bigint] NULL,
	[Health] [int] NOT NULL,
	[WornItemsId] [bigint] NULL,
	[SecurityStamp] VARCHAR(MAX) NULL
 CONSTRAINT [PK_Players] PRIMARY KEY CLUSTERED 
(
	[Id] ASC	
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[WornItemsHistory](
	[Id] [bigint] NOT NULL IDENTITY(1,1),
	[PlayerId] [uniqueidentifier] NOT NULL,
	[HelmetId] [int] NULL,
	[ArmorId] [int] NULL,
	[BeltId] [int] NULL,
	[ShoesId] [int] NULL,
	[Ring1Id] [int] NULL,
	[Ring2Id] [int] NULL,
	[Ring3Id] [int] NULL,
	[Ring4Id] [int] NULL,
	[Ring5Id] [int] NULL,
	[Ring6Id] [int] NULL,
	[Ring7Id] [int] NULL,
	[Ring8Id] [int] NULL,
	[Ring9Id] [int] NULL,
	[Ring10Id] [int] NULL,
	[WeaponLeftId] [int] NULL,
	[WeaponRightId] [int] NULL,
 CONSTRAINT [PK_WornItemsHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Battles] ADD  CONSTRAINT [DF_Battles_Gold]  DEFAULT ((0)) FOR [Gold]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_Damage]  DEFAULT ((0)) FOR [Damage]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_Armor]  DEFAULT ((0)) FOR [Armor]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_Critical]  DEFAULT ((0)) FOR [Critical]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_Anticritical]  DEFAULT ((0)) FOR [Anticritical]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_Dodge]  DEFAULT ((0)) FOR [Dodge]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_Antidodge]  DEFAULT ((0)) FOR [Antidodge]
GO
ALTER TABLE [dbo].[Players] ADD  CONSTRAINT [DF_Players_Gold]  DEFAULT ((0)) FOR [Gold]
GO
ALTER TABLE [dbo].[Players] ADD  CONSTRAINT [DF_Players_Experience]  DEFAULT ((0)) FOR [Experience]
GO
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Items] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [FK_Inventory_Items]
GO
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Players] FOREIGN KEY([PlayerId])
REFERENCES [dbo].[Players] ([Id])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [FK_Inventory_Players]
GO
ALTER TABLE [dbo].[Items]  WITH CHECK ADD  CONSTRAINT [FK_Items_ItemTypes] FOREIGN KEY([TypeId])
REFERENCES [dbo].[ItemTypes] ([Id])
GO
ALTER TABLE [dbo].[Items] CHECK CONSTRAINT [FK_Items_ItemTypes]
GO
ALTER TABLE [dbo].[Players]  WITH CHECK ADD  CONSTRAINT [FK_Players_Battles] FOREIGN KEY([CurrentBattleId])
REFERENCES [dbo].[Battles] ([Id])
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_Players_Battles]
GO
ALTER TABLE [dbo].[Players]  WITH CHECK ADD  CONSTRAINT [FK_Players_WornItemsHistory] FOREIGN KEY([WornItemsId])
REFERENCES [dbo].[WornItemsHistory] ([Id])
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_Players_WornItemsHistory]
GO

ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items] FOREIGN KEY([ArmorId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items1] FOREIGN KEY([BeltId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items1]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items10] FOREIGN KEY([Ring7Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items10]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items11] FOREIGN KEY([Ring8Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items11]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items12] FOREIGN KEY([Ring9Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items12]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items13] FOREIGN KEY([ShoesId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items13]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items14] FOREIGN KEY([WeaponLeftId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items14]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items15] FOREIGN KEY([WeaponRightId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items15]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items2] FOREIGN KEY([HelmetId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items2]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items3] FOREIGN KEY([Ring10Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items3]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items4] FOREIGN KEY([Ring1Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items4]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items5] FOREIGN KEY([Ring2Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items5]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items6] FOREIGN KEY([Ring3Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items6]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items7] FOREIGN KEY([Ring4Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items7]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items8] FOREIGN KEY([Ring5Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items8]
GO
ALTER TABLE [dbo].[WornItemsHistory]  WITH CHECK ADD  CONSTRAINT [FK_WornItemsHistory_Items9] FOREIGN KEY([Ring6Id])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[WornItemsHistory] CHECK CONSTRAINT [FK_WornItemsHistory_Items9]
GO

INSERT INTO [dbo].[ItemTypes] ([Name]) VALUES ('���')
INSERT INTO [dbo].[ItemTypes] ([Name]) VALUES ('������')
INSERT INTO [dbo].[ItemTypes] ([Name]) VALUES ('���')
INSERT INTO [dbo].[ItemTypes] ([Name]) VALUES ('�����')

INSERT INTO [Items] (Name, TypeId, Price, ImagePath, Damage, Armor, Critical, Anticritical, Dodge, Antidodge) VALUES ('��� ������-��', 1, 0, 'path', 15, 0, 5, 2, 20, 20)
INSERT INTO [Items] (Name, TypeId, Price, ImagePath, Damage, Armor, Critical, Anticritical, Dodge, Antidodge) VALUES ('������ ������', 2, 0, 'path', 1, 0, 2, 1, 0, 0)
INSERT INTO [Items] (Name, TypeId, Price, ImagePath, Damage, Armor, Critical, Anticritical, Dodge, Antidodge) VALUES ('������ ����������', 2, 0, 'path', 20, 20, 20, 20, 20, 20)
INSERT INTO [Items] (Name, TypeId, Price, ImagePath, Damage, Armor, Critical, Anticritical, Dodge, Antidodge) VALUES ('������ �������', 2, 0, 'path', 0, 0, 0, 0, 3, 2)
INSERT INTO [Items] (Name, TypeId, Price, ImagePath, Damage, Armor, Critical, Anticritical, Dodge, Antidodge) VALUES ('��������', 3, 0, 'path', 0, 10, 0, 15, 10, 0)
INSERT INTO [Items] (Name, TypeId, Price, ImagePath, Damage, Armor, Critical, Anticritical, Dodge, Antidodge) VALUES ('�������� �����', 4, 0, 'path', 0, 1, 0, 2, 1, 0)

USE [master]
GO
ALTER DATABASE [WebApiTesting] SET  READ_WRITE 
GO