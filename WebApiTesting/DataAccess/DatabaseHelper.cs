﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace WebApiTesting.DataAccess
{
    public static class DatabaseHelper
    {
        public static string connectionString = WebConfigurationManager.ConnectionStrings["DBConString"].ConnectionString;

        public static void ExecuteNonQuery(string sqlExpression, List<SqlParameter> parameters)
        {
            //защита от загрузки javascript в бд
            sqlExpression.Replace("<", "");
            sqlExpression.Replace(">", "");
            using (var connection = new SqlConnection(connectionString)) //ADO.NET автоматически управляет пулом, поэтому вместо создания нового соединения, подбирается свободное из пула не нагружая лишний раз систему
            using (var command = new SqlCommand(sqlExpression, connection))
            {
                connection.Open();
                foreach (var parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
                command.ExecuteNonQuery();
            }
        }
        public static T ReadOneValue<T>(string sqlExpression, List<SqlParameter> parameters)
        {

            /////Select Example
            //string sqlExpression2 = "SELECT Id FROM Players WHERE login=@a";
            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    connection.Open();
            //    SqlCommand command = new SqlCommand(sqlExpression2, connection);
            //    command.Parameters.Add(new SqlParameter("@a", "123"));
            //    SqlDataReader reader = command.ExecuteReader();

            //    if (reader.HasRows) // если есть данные
            //    {
            //        // выводим названия столбцов
            //        //Console.WriteLine("{0}\t{1}\t{2}", reader.GetName(0), reader.GetName(1), reader.GetName(2));

            //        while (reader.Read()) // построчно считываем данные
            //        {
            //            object id = reader.GetValue(0);
            //            //object name = reader.GetValue(1);
            //            //object age = reader.GetValue(2);

            //            //Console.WriteLine("{0} \t{1} \t{2}", id, name, age);
            //        }
            //    }

            //    reader.Close();
            //}

            sqlExpression.Replace("<", "");
            sqlExpression.Replace(">", "");
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand(sqlExpression, connection))
            {
                connection.Open();
                foreach (var parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
                var reader = command.ExecuteReader();
                reader.Read();
                object obj = reader.GetValue(0);
                T value = (T)obj;
                return value;
            }
        }
    }
}