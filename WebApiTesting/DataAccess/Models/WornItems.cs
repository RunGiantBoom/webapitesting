﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTesting.DataAccess.Models
{
    public class WornItems
    {
        public int helmetId;
        public int armorId;
        public int beltId;
        public int shoesId;
        public int ring1Id;
        public int ring2Id;
        public int ring3Id;
        public int ring4Id;
        public int ring5Id;
        public int ring6Id;
        public int ring7Id;
        public int ring8Id;
        public int ring9Id;
        public int ring10Id;
        public int leftweaponId;
        public int rightweaponId;
    }
}