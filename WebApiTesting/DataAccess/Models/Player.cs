﻿using Microsoft.AspNet.Identity;

namespace WebApiTesting.DataAccess.Models
{
    public class Player : IUser
    {
        public Player(string id, string login, string hash, string nick, int gold, int exp, string imagePath, long? currentBattleId, int health, long wornItemsId, string securityStamp)
        {
            Id = id;
            Login = login;
            Hash = hash;
            Nick = nick;
            Gold = gold;
            Exp = exp;
            ImagePath = imagePath;
            CurrentBattleId = currentBattleId;
            Health = health;
            WornItemsId = wornItemsId;
            SecurityStamp = securityStamp;
        }

        public Player(string id, string login, string hash, string nick)
        {
            Id = id;
            Login = login;
            Hash = hash;
            Nick = nick;
        }

        public string Id { get; set; }

        public string UserName 
            {
            get => Login;
            set => Login = value;
        }

        public string Login { get; set; }
        public string Hash { get; set; }
        public string Nick { get; set; }
        public int Gold { get; set; }
        public int Exp { get; set; }
        public string ImagePath { get; set; }
        public long? CurrentBattleId { get; set; }
        public int Health { get; set; }
        public long WornItemsId { get; set; }
        public string SecurityStamp { get; set; }

    }
}