﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTesting.DataAccess.Models
{
    public class ItemStats
    {
        public int Damage = 0;
        public int Armor = 0;
        public double Critical = 0;
        public double Anticritical = 0;
        public double Dodge = 0;
        public double Antidodge = 0;
    }
}