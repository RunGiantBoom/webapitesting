﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WebApiTesting.DataAccess.Models;

namespace WebApiTesting.DataAccess
{
    public class UserStore : IUserStore<Player>, IUserLoginStore<Player>, IUserPasswordStore<Player>, IUserSecurityStampStore<Player>
    {
        private readonly string _connectionString;

        public UserStore(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException("connectionString");

            this._connectionString = connectionString;
        }

        public UserStore()
        {
            this._connectionString = ConfigurationManager.ConnectionStrings["DBConString"].ConnectionString;
        }

        public void Dispose()
        {

        }

        public async Task CreateAsync(Player user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            using (var connection = new SqlConnection(_connectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = $"INSERT INTO [dbo].[Players] " +
                                      $"([Id], [Login], [PasswordHash], [Nickname], " +
                                      $"[Gold], [Experience], [ImagePath], " +
                                      $"[Health]) " +
                                      $"VALUES " +
                                      $"(@Id, " +
                                      $"@Login, " +
                                      $"@PasswordHash, " +
                                      $"@Nick, " +
                                      $"0, " +
                                      $"0, " +
                                      $"'', " +
                                      $"100)";
                var playerId = user.Id;
                var parameter = new SqlParameter
                {
                    ParameterName = "@Id",
                    Value = playerId
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Login",
                    Value = user.Login
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@PasswordHash",
                    Value = user.Hash
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Nick",
                    Value = user.Nick
                };
                command.Parameters.Add(parameter);


                await command.ExecuteNonQueryAsync();


                command.CommandText = "INSERT INTO [dbo].[WornItemsHistory] " +
                                      "([PlayerId]) " +
                                      "VALUES " +
                                      $"(@Id)";


                command.ExecuteNonQuery();



                command.CommandText = "SELECT [Id] " +
                                      "FROM[dbo].[WornItemsHistory] " +
                                      $"WHERE[PlayerId] = @Id";

                var reader = await command.ExecuteReaderAsync();

                if (!await reader.ReadAsync())
                {
                    return;
                }
                var wornItemsId = reader.GetInt64(0);

                reader.Close();

                command.CommandText = "UPDATE [dbo].[Players] " +
                                      $"SET[WornItemsId] = @WornItemsId " +
                                      $"WHERE [Id] = @Id ";

                parameter = new SqlParameter
                {
                    ParameterName = "@WornItemsId",
                    Value = wornItemsId
                };
                command.Parameters.Add(parameter);


                command.ExecuteNonQuery();

            }
        }

        public async Task UpdateAsync(Player user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            using (var connection = new SqlConnection(_connectionString))
            using (var command = connection.CreateCommand())
            {
                var query = "UPDATE [dbo].[Players] " +
                            "SET " +
                            "Login = @Login, " +
                            "PasswordHash = @Hash, " +
                            "Nickname = @Nick, " +
                            "Gold = @Gold, " +
                            "Experience = @Experience, " +
                            "ImagePath = @ImagePath, " +
                            "CurrentBattleId = @CurrentBattleId, " +
                            "Health = @Health, " +
                            "WornItemsId = @WornItemsId, " +
                            "SecurityStamp = @SecurityStamp " +
                            "WHERE [Id] =  @Id";
                await connection.OpenAsync();
                command.CommandText = query;

                var parameter = new SqlParameter
                {
                    ParameterName = "@Login",
                    Value = user.Login
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Hash",
                    Value = user.Hash
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Nick",
                    Value = user.Nick
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Gold",
                    Value = user.Gold
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Experience",
                    Value = user.Exp
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@ImagePath",
                    Value = user.ImagePath ?? ""
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@CurrentBattleId",
                    Value = user.CurrentBattleId
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Health",
                    Value = user.Health
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@WornItemsId",
                    Value = user.WornItemsId
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@SecurityStamp",
                    Value = user.SecurityStamp
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Id",
                    Value = user.Id
                };
                command.Parameters.Add(parameter);

                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task DeleteAsync(Player user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            using (var connection = new SqlConnection(_connectionString))
            using (var command = connection.CreateCommand())
            {
                await connection.OpenAsync();
                command.CommandText = "DELETE FROM [dbo].[Players] WHERE [Id] = " + user.Id;
                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task<Player> FindByIdAsync(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException("userId");

            Player player = null;
            Guid parsedUserId;
            if (!Guid.TryParse(userId, out parsedUserId))
                throw new ArgumentOutOfRangeException("userId", string.Format("'{0}' is not a valid GUID.", new { userId }));

            using (var connection = new SqlConnection(_connectionString))
            using (var command = connection.CreateCommand())
            {
                await connection.OpenAsync();
                command.CommandText = "SELECT Login, PasswordHash, Nickname, Gold, Experience, " +
                                      "ImagePath, CurrentBattleId, Health, WornItemsId, SecurityStamp " +
                                      "FROM [dbo].[Players] " +
                                      $"WHERE Id = @Id";
                var parameter = new SqlParameter
                {
                    ParameterName = "@Id",
                    Value = parsedUserId
                };
                command.Parameters.Add(parameter);
                var reader = await command.ExecuteReaderAsync();
                if (reader.Read())
                {
                    player = new Player(parsedUserId.ToString(), reader.GetString(0), reader.GetString(1),
                        reader.GetString(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetString(5),
                        reader.IsDBNull(6) ? (long?)null : reader.GetInt64(6), reader.GetInt32(7), reader.GetInt64(8),
                        reader.IsDBNull(9)? "": reader.GetString(9));
                }

                return player;
            }
        }

        public async Task<Player> FindByNameAsync(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException(nameof(userName));

            Player player = null;
            using (var connection = new SqlConnection(_connectionString))
            using (var command = connection.CreateCommand())
            {
                await connection.OpenAsync();
                command.CommandText = "SELECT Id, Login, PasswordHash, Nickname, Gold, Experience, " +
                                      "ImagePath, CurrentBattleId, Health, WornItemsId, SecurityStamp " +
                                      "FROM [dbo].[Players] " +
                                      "WHERE Login LIKE @UserName";

                var parameter = new SqlParameter
                {
                    ParameterName = "@UserName",
                    Value = userName
                };
                command.Parameters.Add(parameter);
                var reader = await command.ExecuteReaderAsync();
                if (await reader.ReadAsync())
                {
                    player = new Player(reader.GetGuid(0).ToString(), reader.GetString(1), reader.GetString(2),
                        reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5), reader.GetString(6),
                        reader.IsDBNull(7) ? (long?)null : reader.GetInt64(7), reader.GetInt32(8), reader.GetInt64(9),
                        reader.IsDBNull(10) ? "" : reader.GetString(10));
                }

                return player;
            }
        }

        //Is needed when we use external logins
        public Task AddLoginAsync(Player user, UserLoginInfo login)
        {
            return new Task(() => { });
        }

        //Is needed when we use external logins
        public Task RemoveLoginAsync(Player user, UserLoginInfo login)
        {
            return new Task(() => { });
        }

        //Is needed when we use external logins
        public async Task<IList<UserLoginInfo>> GetLoginsAsync(Player user)
        {
            return null;
        }

        public async Task<Player> FindAsync(UserLoginInfo login)
        {
            return null;
        }

        public async Task SetPasswordHashAsync(Player user, string passwordHash)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            user.Hash = passwordHash;
        }

        public Task<string> GetPasswordHashAsync(Player user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.Hash);
        }

        public Task<bool> HasPasswordAsync(Player user)
        {
            return Task.FromResult(!string.IsNullOrEmpty(user.Hash));
        }

        public Task SetSecurityStampAsync(Player user, string stamp)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            user.SecurityStamp = stamp;

            return Task.FromResult(0);
        }

        public Task<string> GetSecurityStampAsync(Player user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.SecurityStamp);
        }
    }
}