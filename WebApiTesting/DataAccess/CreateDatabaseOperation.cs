﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace WebApiTesting.DataAccess
{
    public class CreateDatabaseOperation
    {
        private static CreateDatabaseOperation _instance;
        public static CreateDatabaseOperation Instance => _instance ?? (_instance = new CreateDatabaseOperation());

        private string _path = null;//set here your own path MS SQL Server data if you need
        private const string DatabaseName = "WebApiTesting";

        private CreateDatabaseOperation()
        {

        }

        public void Execute(string connectionString, bool deleteIfExist)
        {
            if (string.IsNullOrWhiteSpace(_path))
            {
                _path = "C:\\Program Files\\Microsoft SQL Server\\MSSQL10.MSSQLSERVER\\MSSQL\\DATA";
            }

            var query = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("/database.sql"));
            query = query.Replace("{Path}", _path);
            query = query.Replace("\r", " ");
            query = query.Replace("\n", " ");

            var isDatabaseExists = IsDatabaseExists(connectionString);

            if (isDatabaseExists && deleteIfExist)
            {
                using (var connection = new SqlConnection(connectionString))
                using (var command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText = $"ALTER DATABASE {DatabaseName} SET SINGLE_USER WITH ROLLBACK IMMEDIATE; " +
                                          $"DROP DATABASE {DatabaseName}";
                    command.ExecuteNonQuery();
                    
                }
            }

            if (isDatabaseExists && !deleteIfExist) return;

            using (var connection = new SqlConnection(connectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                var subQueries = query.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var subQuery in subQueries)
                {
                    command.CommandText = subQuery;
                    command.ExecuteNonQuery();
                }
            }
        }

        private bool IsDatabaseExists(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand($"SELECT db_id('{DatabaseName}')", connection))
                {
                    try
                    {
                        connection.Open();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    return (command.ExecuteScalar() != DBNull.Value);
                }
            }
        }

    }
}